export interface IRecord2 {
  userId: number,
  id: number,
  title: string,
  body: string;
}
