import { Component, OnInit } from '@angular/core';
import { IRecord1 } from './models/record1';
import { IRecord2 } from './models/record2';
import { DatabaseService } from './services/database.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
	title: String = 'Hello Bobby';
	records1: IRecord1;
	records2: IRecord2;

	// pass in the Database Service through the constructor for methods to execute
	constructor(private databaseService: DatabaseService) { }

	ngOnInit() {
		// => (fat arrow / lambda example) equivalant to
		// this.records = this.databaseService.getData();

		this.databaseService.getData1().subscribe(data => {
			this.records1 = data;
		});

		this.databaseService.getData2().subscribe(data => {
			this.records2 = data;
		});
	}
}
