// https://www.youtube.com/watch?v=fh2GyYQcuxU
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IRecord1 } from '../models/record1';
import { IRecord2 } from '../models/record2';

const ZERO = 0;
const ONE = 1;
const TWO = 2;

@Injectable()
export class DatabaseService {

	url0: string = 'https://www.cv64.us/cv64/database.json';
	url1: string = 'http://localhost:4200/assets/data/database.json';
	url2: string = 'https://jsonplaceholder.typicode.com/posts';
	urls: Array<string> = [];

	constructor(private httpClient: HttpClient) {
		console.log('DatabaseService.constructor');
		//this.urls = [this.url0, this.url1, this.url2];
		this.urls.push(this.url0);
		this.urls.push(this.url1);
		this.urls.push(this.url2);
		console.log('urls length: ' + this.urls.length);
	}

	// subscribe - handle async processing, returns an observable
	getData1() {
		console.log('DatabaseService.getData1');
		return this.httpClient.get<IRecord1>(this.urls[ONE]);
	}

	// subscribe - handle async processing, returns an observable
	getData2() {
		console.log('DatabaseService.getData2');
		//	console.log(this.httpClient.get<IRecord2>(this.url2));
		return this.httpClient.get<IRecord2>(this.urls[TWO]);
	}
}
