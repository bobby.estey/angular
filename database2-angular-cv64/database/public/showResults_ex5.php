<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- Popper JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <style media="screen">

  </style>
  <title>Ex5 - PHP Page</title>
</head>
<body>
  <div class="container-fluid">
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
      <!-- Brand -->
      <a class="navbar-brand" href="#">Logo</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <!-- Links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="#">Home</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle active" href="#" data-toggle="dropdown">
              Info Tech
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="ex5_form.html">Form</a>
              <a class="dropdown-item active" href="#">Results</a>
            </div>
          </li>
          <!-- Dropdown -->
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
              Interests
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="#">Interest 1</a>
              <a class="dropdown-item" href="#">Interest 2</a>
              <a class="dropdown-item" href="#">Interest 3</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">About</a>
          </li>
        </ul>
      </div>
    </nav>
    <div>
      <?php
      // display in browser the value of $_POST associative array as initial test
      // use echo function and its parameter needs to be a string and use . operator to combine strings
      echo ("Info Tech Tool 1 = " . $_POST['tool1']);
      echo ("<br>");
      echo ("Info Tech Tool 2 = " . $_POST['tool2']);
      echo ("<br>");
      echo ("Info Tech Tool 3 = " . $_POST['tool2']);
      echo ("<br>");

      // use function displayPostArray to be able to display contents of $_POST
      // in a way that is flexible in terms of items stored in $_POST
      displayPostArray($_POST);

      // define functions ... it is okay that they are defined after they are being called
      //
      function displayPostArray ($postarray) {
      	// echo ("displayPostArray.<br>");

      	// want to loop through each item of associative array
      	// Use of => is similar to the regular = assignment operator,
      	// except that you are assigning a value to an index and not to a variable.
      	// "as" is used to assign a specific element of array to variable $tool
      	// and => is used to assign value associated with $tool to the variable $score
      	foreach ($postarray as $tool => $score)
      	{
      		echo "$tool" . " = " . "$score<br>";
      	}
      	//
      }
      ?>
    </div>
  </div>
</body>
</html>
