<?php
// NOTE:  json reply - only echo when testing and do not include in response or exception will occur
// core.es5.js:1020 ERROR SyntaxError: Unexpected token C in JSON at position 0 at JSON.parse (<anonymous>)
header('Content-Type: application/json');

require_once('db_config.php');

$databaseConnection=$config['databaseConnection'];

// Create connection
$connection = new mysqli($databaseConnection['servername'], $databaseConnection['username'], $databaseConnection['password'], $databaseConnection['database']);

// Check connection
if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
}
//echo "Connected successfully";

// The query we want to execute
$sql = "SELECT * FROM table1";

$results = $connection->query($sql);
$rows = array();

if ($results->num_rows > 0) {
  while($r = mysqli_fetch_assoc($results)) {
    $rows[] = $r;
  }

  print json_encode($rows);

} else {
    echo "0 results";
}

$results->close();
$connection->close();
?>

