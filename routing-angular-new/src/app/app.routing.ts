import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { SettingsComponent } from './settings/settings.component';

const routes: Routes = [
	{ path: '', component: HomeComponent },
	{ path: 'profile', component: ProfileComponent },
	{ path: 'settings', component: SettingsComponent }
];

@NgModule({
	declarations: [
		HomeComponent,
		ProfileComponent,
		SettingsComponent
	],
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [RouterModule],
	providers: []

})
export class AppRoutingModule { }
