import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Cv64Component } from './cv64.component';

describe('Cv64Component', () => {
  let component: Cv64Component;
  let fixture: ComponentFixture<Cv64Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Cv64Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Cv64Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
