import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './layouts/home/home.component';
import { AboutComponent } from './layouts/about/about.component';
import { Cv64Component } from './layouts/cv64/cv64.component';
import { PropertiesComponent } from './layouts/properties/properties.component';
import { SoftwareComponent } from './layouts/software/software.component';
import { TrComponent } from './layouts/tr/tr.component';

const routes: Routes = [
	{ path: '', component: HomeComponent },
	{ path: 'about', component: AboutComponent },
	{ path: 'cv64', component: Cv64Component },
	{ path: 'properties', component: PropertiesComponent },
	{ path: 'software', component: SoftwareComponent },
	{ path: 'tr', component: TrComponent }
];

@NgModule({
	declarations: [
		HomeComponent,
		AboutComponent,
		Cv64Component,
		PropertiesComponent,
		SoftwareComponent,
		TrComponent
	],
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [RouterModule],
	providers: []

})
export class AppRoutingModule { }
