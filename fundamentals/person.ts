export class Person {

	firstName: string;
	lastName: string;

	constructor(firstName, lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public getFullName() {
		return this.firstName + " " + this.lastName;
	}
}

class Student extends Person {
	
	bobby = new Person("Bobby", "Estey");
	bobby.getFullName();
}