import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AngularFirestoreDocument } from 'angularfire2/firestore';

@Component({
  selector: 'app-framework-poll',
  templateUrl: './framework-poll.component.html',
  styleUrls: ['./framework-poll.component.css'],
  encapsulation: ViewEncapsulation.Native
})
export class FrameworkPollComponent implements OnInit {

angularVoteCount: number;
reactVoteCount: number;
vueVoteCount: number;
hasVoted = false;
updating = false;
fsRef: AngularFirestoreDocument<any>;

  constructor(private afs: AngularFirestore) { }

  ngOnInit() {
    this.fsRef = this.afs.doc('framworkPoll/current');
    this.fsRef.valueChanges().subscribe(doc => {
      this.angularVoteCount = doc.angularVoteCount;
      this.reactVoteCount = doc.reactVoteCount;
      this.vueVoteCount = doc.vueVoteCount;
    });
  }

}
