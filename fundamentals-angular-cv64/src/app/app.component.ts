import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Item } from './models/item.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, Item {

  title = 'fundamentals-angular-cv64';
  anotherVariable = 'anotherVariable1';
  item: Item = <Item>{};
  items: Item[] = [];
  private arrayIndex = 1;

  constructor() {
    console.log('fundamentals - app.component.ts - constructor');
  }

  ngOnInit() {
    console.log('fundamentals - app.component.ts - ngOnInit');
    this.simpleList();
    this.item = this.items[0];
  }

  simpleList() {
    console.log('fundamentals - app.component.ts - simpleList');
    this.item = { description: 'description1', title: 'title1' };
    this.items.push(this.item);

    this.item = { description: 'description2', title: 'title2' };
    this.items.push(this.item);
  }

  nextItem() : void {
    if(this.arrayIndex <= 2) {
      this.item = this.items[this.arrayIndex];
      this.arrayIndex++;
    } else {
      this.item = this.items[0];
      this.arrayIndex = 1;
    }
  }
}
