import { Component, OnInit, Input } from '@angular/core';
import { Item } from './../models/item.model';

@Component({
  selector: 'app-subcomponent',
  templateUrl: './subcomponent.component.html',
  styleUrls: ['./subcomponent.component.css']
})
export class SubcomponentComponent implements OnInit {

  @Input() anotherVariable;
  @Input() items: Item[] = [];

  constructor() {
    console.log('fundamentals - subcomponent.component.ts - constructor');
   }

  ngOnInit() {
    console.log('fundamentals - subcomponent.component.ts - ngOnInit');
  }

}
